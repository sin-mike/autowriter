from uuid import uuid1
from jinja2 import Template
import datetime


def main():
    xtable = [0.216, 11.057, 22.289, 33.251, 43.978, 54.981, 66.114, 76.937, 88.054]
    temperatures = [22, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70]

    head = """;; SCRIPT UUID = {{UUID}}
;; ESTIMATED RUNTIME = {{RUNTIME}}
SCRIPT START
REALTIME({{REALTIME}})
LIVETIME({{LIVETIME}})

"""
    head = Template(head)

    section = """
TEMP({{TEMPERATURE}})
WAIT({{WAIT}})

"""
    section = Template(section)

    body = """
;; {{PREFIX}}
MPA3 CLEAR
DATA_INFO({{DATANAME}},{{USERNAME}},,,,,,,,,)
MM({{POSX}},{{POSY}})
MPA3 START
WAIT FOR MPA3
MPA3 STORE()

"""
    body = Template(body)

    tail = """
TEMP(22)
SCRIPT END

"""
    tail = Template(tail)

    uuid = str(uuid1())
    REALTIME = 300
    LIVETIME = REALTIME+2
    BIAS = 15

    script = ""
    ert = 0

    for i, t in enumerate(temperatures):
        wait = 30 * 60
        script += section.render({
                "TEMPERATURE": t, 
                "WAIT": wait
            })
        ert += wait
        for j, x in enumerate(xtable):
            script += body.render({
                "PREFIX": "%d of %d" % (i * len(xtable) + j + 1, len(temperatures) * len(xtable)),
                "DATANAME": "ChupinLipid12 Sample %d Temperature %d SC2 SOURCE %s" % (j + 1, t, uuid),
                "POSX": x,
                "POSY": 0.0,
                "USERNAME": "PYWRITER"
            })
            ert+=REALTIME+BIAS

    script = head.render({
            "UUID": uuid,
            "RUNTIME":datetime.timedelta(seconds=ert),
            "REALTIME":REALTIME,
            "LIVETIME":LIVETIME
        }) + script

    script+=tail.render()
    with open("../scripts/"+str(datetime.datetime.now().strftime('%Y%m%d'))+"_SINTSOV_Chupin12.dat","w") as f:
        f.write(script)




    # with open("chupin12.dat","w") as f:
if __name__ == '__main__':
    main()
