import pyodbc
from datetime import datetime as dt


def initSQL():
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=93.175.16.6;DATABASE=RockMaker;UID=guest;PWD=Lurige17')
    username = cnxn.getinfo(pyodbc.SQL_USER_NAME)
    if username:
        print "connected successfully with username %s" %username
    cursor = cnxn.cursor()
    return cursor

def convertRegion2WellName(cursor,regionID):
    request = """SELECT * FROM [RockMaker].[dbo].[Region] where [ID] = %d """%regionID
    cursor.execute(request)
    res = cursor.fetchone()
    wellDropID = res[1]

    request = """SELECT *  FROM [RockMaker].[dbo].[WellDrop] where [ID] = %d """%wellDropID
    cursor.execute(request)
    res = cursor.fetchone()
    wellID = res[1]
    
    request = """SELECT *  FROM [RockMaker].[dbo].[Well] where [ID] = %d """%wellID
    cursor.execute(request)
    res = cursor.fetchone()
    rowLetter = res[3]
    columnNumber = res[4]
    
    return rowLetter,columnNumber

def getPlateCoordinates(plateID):
    cursor = initSQL()
    print "fetching data for plate ID %d" %plateID
    request = """SELECT * FROM [RockMaker].[dbo].[ExperimentPlate] where [PlateID] = %d"""%plateID
    cursor.execute(request)
    res = cursor.fetchone()
    print res
    experimentPlateID = res[0]
    print "corresponding experimentPlateID = %d"%experimentPlateID

    request = """SELECT * FROM [RockMaker].[dbo].[ImagingTask] where [ExperimentPlateID] = %d"""%experimentPlateID
    cursor.execute(request)
    res = cursor.fetchall()
    res = [r for r in res if r[5] is not None]
    res = sorted(res, key=lambda tup: tup[5], reverse=True)

    answer = []
    captureProfiles = {6: "FRAP-HT", 7:"FRAP-FR", 32:"SAXS coordinates"}
    for imagingTaskID,_,_,_,_,dateImaged,_,_,_ in res:
        print "imagingTaskID = %d imaged %s"%(imagingTaskID,str(dateImaged))

        request = """SELECT * FROM [RockMaker].[dbo].[ImageBatch] where [ImagingTaskID] = %d """%imagingTaskID
        cursor.execute(request)
        res2 = cursor.fetchone()
        if res2 is None or len(res2) == 0:
            print "no data in [ImageBatch] for request\n %s"%request
            continue
        imageBatchID = res2[0]
        print "corresponding imageBatchID = %d"%imageBatchID

        resMax = []
        nameMax = None
        for (captureProfileVersionID, captureProfileName) in captureProfiles.items():
            request = """SELECT *  FROM [RockMaker].[dbo].[CaptureResult] where [ImageBatchID] = %d and [captureProfileVersionID] = %d """%(imageBatchID,captureProfileVersionID)
            cursor.execute(request)
            res2 = cursor.fetchall()
            
            if (res2 is None) or (len(res2)==0):
                print "no data in [CaptureResult] for requests: \n %s"%request
                continue
            else:
                print "found %d coordinates in [CaptureResult] for requests: \n %s"%(len(res2),request)
                if len(res2) > len(resMax):
                    resMax = res2
                    nameMax = captureProfileName
                else:
                    continue

        res2 = resMax

        if res2 is None or len(res2) == 0:
            print "no data in [CaptureResult] for requests:", captureProfiles.values()
            continue
        else:
            print "found %d coordinates for capture profile %s"%(len(res2), nameMax)
            for _,regionID,_,_,xCenter,yCenter,_,_,_ in res2:
                answer.append((convertRegion2WellName(cursor,regionID), (xCenter, yCenter)))
            break
    if len(answer)>0:
        return answer
    else:
        return None



if __name__ == '__main__':

    plateID = 562
    print getPlateCoordinates(plateID)
    




    
