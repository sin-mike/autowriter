#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
from optparse import OptionParser
import sys
import csv
import pprint
import string
import ConfigParser
from datetime import datetime as dt
import os
import shutil
import datetime
import uuid

def getCalibration(Config):
    print "configuration sections:",
    print Config.sections()
    xf1 = float(Config.get("FRAP","x1"))
    yf1 = float(Config.get("FRAP","y1"))
    xf2 = float(Config.get("FRAP","x2"))
    yf2 = float(Config.get("FRAP","y2"))
    xf3 = float(Config.get("FRAP","x3"))
    yf3 = float(Config.get("FRAP","y3"))

    xs1 = float(Config.get("SAXS","x1"))
    ys1 = float(Config.get("SAXS","y1"))
    xs2 = float(Config.get("SAXS","x2"))
    ys2 = float(Config.get("SAXS","y2"))
    xs3 = float(Config.get("SAXS","x3"))
    ys3 = float(Config.get("SAXS","y3"))

    a = -((-(xs2*yf1) + xs3*yf1 + xs1*yf2 - xs3*yf2 - xs1*yf3 + xs2*yf3)/(xf2*yf1 - xf3*yf1 - xf1*yf2 + xf3*yf2 + xf1*yf3 - xf2*yf3))
    b = -((xf2*xs1 - xf3*xs1 - xf1*xs2 + xf3*xs2 + xf1*xs3 - xf2*xs3)/(-(xf2*yf1) + xf3*yf1 + xf1*yf2 - xf3*yf2 - xf1*yf3 + xf2*yf3))
    c = -((-(xf3*xs2*yf1) + xf2*xs3*yf1 + xf3*xs1*yf2 - xf1*xs3*yf2 - xf2*xs1*yf3 + xf1*xs2*yf3)/(-(xf2*yf1) + xf3*yf1 + xf1*yf2 - xf3*yf2 - xf1*yf3 + xf2*yf3))
    d = -((yf2*ys1 - yf3*ys1 - yf1*ys2 + yf3*ys2 + yf1*ys3 - yf2*ys3)/(xf2*yf1 - xf3*yf1 - xf1*yf2 + xf3*yf2 + xf1*yf3 - xf2*yf3))
    e = -((-(xf2*ys1) + xf3*ys1 + xf1*ys2 - xf3*ys2 - xf1*ys3 + xf2*ys3)/(xf2*yf1 - xf3*yf1 - xf1*yf2 + xf3*yf2 + xf1*yf3 - xf2*yf3))
    f = -((-(xf3*yf2*ys1) + xf2*yf3*ys1 + xf3*yf1*ys2 - xf1*yf3*ys2 - xf2*yf1*ys3 + xf1*yf2*ys3)/(xf2*yf1 - xf3*yf1 - xf1*yf2 + xf3*yf2 + xf1*yf3 - xf2*yf3))

    def fx(x,y):
        return a*x + b*y + c
    def fy(x,y):
        return d*x + e*y + f
    return fx,fy

def sort(data, preference = "SHORT_TRAVEL"):
    def order(rw):
        row = ord(rw[0])-65
        col = int(rw[1:])-1

        if preference == "SHORT_TRAVEL":
            if row % 2 == 0:
                return row*12+col
            else:
                return (row+1)*12-col
        elif preference == "ROW_FIRST":
            return row*12+col
        elif preference == "COLUMN_FIRST":
            return col*8+row

    print "sorting data:" 
    pp.pprint(data)
    data = sorted(data, key = lambda d:order(d[0]))
    print "sorted data:"
    pp.pprint(data)
    return data

if __name__ == '__main__':
    stime = dt.now()
    global pp
    pp = pprint.PrettyPrinter(indent=4)
    parser = OptionParser()
    parser.add_option("-o", "--output", dest="output", help="write output to FILE", metavar="FILE")
    parser.add_option("-i", "--input", dest="input", help="take coordinates from FILE", metavar="FILE")
    parser.add_option("-s", "--settings", dest="settings", help="take settings from FILE", metavar="FILE")
    parser.add_option("-c", "--script", dest = "script", help="write script to FILE", metavar="FILE")
    parser.add_option("-p", "--plate", dest = "plateID", help="specify the ID of plate to fetch", metavar="INT")
    (opts,args) = parser.parse_args()

    if opts.settings is None:
        settingsPath = 'default.txt'
    else:
        settingsPath = opts.settings
    print("settings path is %s"%settingsPath)
    Config = ConfigParser.ConfigParser()
    Config.read(settingsPath)


    if opts.input is None and opts.plateID is None:
        print("no input file specified")
        exit(-1)

    if opts.input is not None and opts.plateID is not None:
        print("both input file and plateID specified")
        exit(-1)

    if opts.input is not None:
        plateID = None
        print("processing data for %s"%opts.input)

    if opts.plateID is not None:
        plateID = int(opts.plateID)
        print("processing data for plate %d"%plateID)

    if opts.output is None:
        outpath = "coordinates.csv"
        print("output file is %s"% outpath)

    if opts.script is None:
        postfix = os.path.split(opts.input)[-1]
        postfix = os.path.splitext(postfix)[0]
        scriptName = str(dt.now().strftime('%Y%m%d'))+"_"+Config.get("Script", "name")+"_"+postfix+"_autowriter.dat"

        scriptPath = os.path.join(Config.get("General","scriptLocalFolder"),scriptName)
        print("script file is %s"% scriptPath)
    else:
        scriptPath = opts.script

    
    fx,fy = getCalibration(Config)

    scale = float(Config.get("General","scale"))
    print("scale = %f"%scale)
    repeat = int(Config.get("General","repeat"))
    print("repeat = %d"%repeat)

    fig = plt.figure()
    plt.axis("equal")
    plt.axis([-20,120,-75,10])

    

    if plateID is None:
        plateID = int(Config.get("General", "plateID"))
    ax = float(Config.get("FRAP", "ax"))
    ay = float(Config.get("FRAP", "ay"))

    print("FRAP steps ax = %f, ay = %f"%(ax,ay))

    data = []
    position = []
    alphabet = {"A":1,"B":2,"C":3,"D":4,"E":5,"F":6,"G":7,"H":8}


    if opts.input is not None:
        with open(opts.input,'r') as csvfile:
            coordFRAP = csv.reader(csvfile, delimiter=',')
            for row in coordFRAP:
                rw = row[0]
                if len(rw) == 2:
                    pos = (int(rw[1])-1,alphabet[rw[0]]-1)
                else:
                    pos = (int(rw[1])*10+int(rw[2])-1,alphabet[rw[0]]-1)
                if len(row[1]) == 0 or len(row[2]) == 0: 
                    continue
                abspos = (float(row[1])*scale+ax*pos[0],float(row[2])*scale-ay*pos[1])
                position.append(pos)
                data.append([row[0],fx(abspos[0], abspos[1]),fy(abspos[0], abspos[1])])
                circle=plt.Circle(abspos,.2,color='r', clip_on = True)
                fig.gca().add_artist(circle)
                # print(rw),
                # print(pos),
                # print(abspos)
                # fig.show()
                # raw_input()

    if opts.plateID is not None:
        import sql
        coordFRAP = sql.getPlateCoordinates(plateID)
        for rw,xy in coordFRAP:
            if len(rw) == 2:
                    pos = (int(rw[1])-1,alphabet[rw[0]]-1)
            else:
                pos = (int(rw[1])*10+int(rw[2])-1,alphabet[rw[0]]-1)
            abspos = (float(xy[0])*0.001+ax*pos[0],float(xy[1])*0.001-ay*pos[1])
            position.append(pos)
            data.append([rw[0]+str(rw[1]),fx(abspos[0], abspos[1]),fy(abspos[0], abspos[1])])
            circle=plt.Circle(abspos,.2,color='r', clip_on = True)
            fig.gca().add_artist(circle)

    data = sort(data, preference= "SHORT_TRAVEL")

    for i in range(8):
        for j in range(12):
            circle=plt.Circle((j*ax,-i*ay),2.5,edgecolor = "k",facecolor = "None", clip_on = True)
            fig.gca().add_artist(circle)

    # plt.axis("equal")
    # plt.axis([-10,120,-70,10])
    # fig.title("input coordinates")
    fig.savefig("out.png", dpi = 220)
    fig.show()

    for row in data:
        row[1] = round(row[1],3)
        row[2] = round(row[2],3)

    print("absolute position for SAXS:")
    pp.pprint(data)

    print "writing to %s with repeat = %d..."%(outpath,repeat),
    with open(outpath, 'wb') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for i in range(repeat):
            for row in data:
                writer.writerow(row)
    print "done!"

    etr = 0
    print "writing script to %s with repeat = %d"%(scriptPath,repeat)
    with open(scriptPath, 'w') as script:
        l = len(data)
        nmax = l*repeat
        livetime = int(Config.get("Script","livetime"))
        nstart = int(Config.get("Script", "start"))
        name = Config.get("Script", "name")
        time = str(dt.now())
        realtime = int(Config.get("Script","realtime"))
        serviceTime = float(Config.get("General","serviceTimePerWell"))

        etr = int(nmax*(float(realtime)+serviceTime))

        uuid = uuid.uuid1()
        script.write(";; script uuid = %s\n"%str(uuid))
        script.write(";; estimated script runtime %s\n\n"%str(datetime.timedelta(seconds=etr)))
        script.write("SCRIPT START\n")
        script.write("REALTIME(%s)\n"%Config.get("Script","realtime"))

        script.write("LIVETIME(%d)\n"%livetime)
        for i in range(repeat):
            for j,row in enumerate(data):
                scriptLine = i*l+j+nstart+1
                script.write("\n")
                script.write(";; %d of %d\n"%(i*l+j+nstart,nmax+nstart))
                script.write("MPA3 CLEAR\n")
                script.write("DATA_INFO(%s SC1 plateID %d %d of %d source %s,%s,%s,,,,,,,,)\n"%(row[0],plateID,scriptLine,nmax+nstart,str(uuid),name,time))                
                script.write("MM(%.3f,%.3f)\n"%(row[1],row[2]))
                script.write("MPA3 START\nWAIT FOR MPA3\nMPA3 STORE()\n")
                print("script line %d of %d"%(scriptLine,nmax+nstart))

        script.write("\nSCRIPT END")


    try:
        shutil.copyfile(scriptPath,os.path.join(str(Config.get("General","scriptDestinationFolder")),scriptName))
        print "script file %s was successfully copied to %s"%(scriptPath, Config.get("General","scriptDestinationFolder"))
    except Exception, e:
        print "ERROR: unable to copy file to destination folder:",
        print e

    print "finished, time elapsed %s" %str(dt.now()-stime)
    print "estimated script timerun %s"%str(datetime.timedelta(seconds=etr))
    # raw_input("Press any key")