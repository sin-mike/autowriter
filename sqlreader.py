import pyodbc
from datetime import datetime as dt

class sqlreader(object):
    ip = None
    cursorRM = None
    cnxnRM = None
    cursorRI = None
    cnxnRI = None

    def __init__(self,ip, login, password):
        self.ip = ip
        cnxnRM = pyodbc.connect('DRIVER={SQL Server};SERVER=%s;DATABASE=RockMaker;UID=%s;PWD=%s'%(ip,login,password))
        cnxnRI = pyodbc.connect('DRIVER={SQL Server};SERVER=%s;DATABASE=RockImager;UID=%s;PWD=%s'%(ip,login,password))
        username = cnxnRM.getinfo(pyodbc.SQL_USER_NAME)
        if username:
            print "connected successfully with username %s" %username
        self.cursorRM = cnxnRM.cursor()
        self.cnxnRM = cnxnRM
        self.cursorRI = cnxnRI.cursor()
        self.cnxnRI = cnxnRI


    def getDropConfig(self, PlateConfigID):
        pass


    def convertRegion2WellName(self,regionID):
        cursor = self.cursorRM
        request = """SELECT * FROM [RockMaker].[dbo].[Region] where [ID] = %d """%regionID
        cursor.execute(request)
        res = cursor.fetchone()
        wellDropID = res[1]

        request = """SELECT *  FROM [RockMaker].[dbo].[WellDrop] where [ID] = %d """%wellDropID
        cursor.execute(request)
        res = cursor.fetchone()
        wellID = res[1]
        
        request = """SELECT *  FROM [RockMaker].[dbo].[Well] where [ID] = %d """%wellID
        cursor.execute(request)
        res = cursor.fetchone()
        rowLetter = res[3]
        columnNumber = res[4]
        
        return rowLetter,columnNumber

    def getPlateCoordinates(self,plateID):
        cursor = self.cursorRM
        print "fetching data for plate ID %d" %plateID
        request = """SELECT * FROM [RockMaker].[dbo].[ExperimentPlate] where [PlateID] = %d"""%plateID
        cursor.execute(request)
        res = cursor.fetchone()
        experimentPlateID = res[0]
        print "corresponding experimentPlateID = %d"%experimentPlateID

        request = """SELECT * FROM [RockMaker].[dbo].[ImagingTask] where [ExperimentPlateID] = %d"""%experimentPlateID
        cursor.execute(request)
        res = cursor.fetchall()
        res = [r for r in res if r[5] is not None]
        res = sorted(res, key=lambda tup: tup[5], reverse=True)

        answer = []
        for imagingTaskID,_,_,_,_,dateImaged,_,_,_ in res:
            print "imagingTaskID = %d imaged %s"%(imagingTaskID,str(dateImaged))

            request = """SELECT * FROM [RockMaker].[dbo].[ImageBatch] where [ImagingTaskID] = %d """%imagingTaskID
            cursor.execute(request)
            res2 = cursor.fetchone()
            imageBatchID = res2[0]
            print "corresponding imageBatchID = %d"%imageBatchID

            captureProfileVersionID = 31
            request = """SELECT *  FROM [RockMaker].[dbo].[CaptureResult] where [ImageBatchID] = %d and [captureProfileVersionID] = %d """%(imageBatchID,captureProfileVersionID)
            cursor.execute(request)
            res2 = cursor.fetchall()

            if len(res2) == 0:
                print "no data in [CaptureResult] for request\n %s"%request
                continue
            else:
                for _,regionID,_,_,xCenter,yCenter,_,_,_ in res2:
                    answer.append((self.convertRegion2WellName(regionID), (xCenter, yCenter)))
                break
        if len(answer)>0:
            return answer
        else:
            return None



if __name__ == '__main__':

    plateID = 562
    sql = sqlreader("93.175.16.6","guest","Lurige17")
    print sql.getPlateCoordinates(plateID)
    




    
